#!/bin/bash
function isMatched() {
   	local param=$1
   	local existingKeysDataAsString="${@:2}"

	shopt -s nocasematch

   	echo >&2 "\t\tparameter to be compared is $param against >$existingKeysDataAsString< [$@]" 
	local matched="false"

	local existingKeysDataArr=$(echo $existingKeysDataAsString | tr " " "\n")
	for volumeExistingkey in $existingKeysDataArr
   	do
	   echo >&2 "\t\tchecking $param against $volumeExistingkey"

	   case $param in
	   		$volumeExistingkey)
			   	matched="true"
				break
				;;
			*)
				matched="false"
				;;	   
	   esac
	   
   	done

	if [ "$matched" == "true" ]; then
		echo "matched"
	else
		echo "unmatched"
	fi		

}
$@